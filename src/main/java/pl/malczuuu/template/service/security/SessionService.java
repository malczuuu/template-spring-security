package pl.malczuuu.template.service.security;

import pl.malczuuu.template.service.common.error.UnauthorizedException;

public interface SessionService {

  SessionDto createSession(CredentialsDto dto) throws UnauthorizedException;

  SessionDto getSession(String token) throws UnauthorizedException;

  SessionDto deleteSession(String token) throws UnauthorizedException;
}
