package pl.malczuuu.template.service.security.engine;

import com.google.common.collect.ImmutableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import pl.malczuuu.template.service.common.error.ServiceException;
import pl.malczuuu.template.service.security.UserDto;
import pl.malczuuu.template.service.security.UserService;

/**
 * Service used for mapping authenticated user to {@link Principal} object.
 */
@Service
class PrincipalService implements UserDetailsService {

  private final UserService userService;

  @Autowired
  PrincipalService(UserService userService) {
    this.userService = userService;
  }

  @Override
  public Principal loadUserByUsername(String username) throws UsernameNotFoundException {
    try {
      return loadUserByUsernameImpl(username);
    } catch (ServiceException e) {
      throw new UsernameNotFoundException(username);
    }
  }

  private Principal loadUserByUsernameImpl(String username) throws ServiceException {
    UserDto user = userService.getUser(username);
    return new Principal(
        user.getUsername(),
        user.getPassword(),
        !user.isDeleted(),
        true,
        !user.isDeleted(),
        true,
        user.getRole().getAuthorities());
  }
}
