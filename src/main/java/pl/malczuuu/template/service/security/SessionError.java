package pl.malczuuu.template.service.security;

import pl.malczuuu.template.service.common.error.NamedError;

public enum SessionError implements NamedError {
  NOT_FOUND("Session.NotFound"),
  UNAUTHORIZED("Session.Unauthorized");

  private final String name;

  SessionError(String name) {
    this.name = name;
  }

  @Override
  public String getName() {
    return name;
  }
}
