package pl.malczuuu.template.service.security;

import pl.malczuuu.template.service.common.model.Mapper;

public class SessionMapper implements Mapper<Session, SessionDto> {

  private UserMapper userMapper = new UserMapper();

  @Override
  public SessionDto map(Session session) {
    return SessionDto.builder()
        .token(session.getToken())
        .user(userMapper.map(session.getUser()))
        .expiredAt(session.getExpiredAt())
        .createdAt(session.getCreatedAt())
        .build();
  }
}
