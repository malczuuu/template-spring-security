package pl.malczuuu.template.service.security;

import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/api/sessions")
class SessionController {

  private static final Logger log = LoggerFactory.getLogger(SessionController.class);

  private final SessionService sessionService;

  @Autowired
  SessionController(SessionService sessionService) {
    this.sessionService = sessionService;
  }

  @PostMapping(produces = "application/json", consumes = "application/json")
  public ResponseEntity<SessionDto> sessionPost(@RequestBody @Valid CredentialsDto dto) {
    SessionDto session = sessionService.createSession(dto);
    log.info("Created session={}", session);
    return ResponseEntity.ok(session);
  }

  @GetMapping(path = "/{token}", produces = "application/json")
  public ResponseEntity<SessionDto> sessionGetToken(@PathVariable String token) {
    SessionDto session = sessionService.getSession(token);
    log.info("Retrieved session={}", session.getToken());
    return ResponseEntity.ok(session);
  }

  @DeleteMapping(path = "/{token}", produces = "application/json")
  public ResponseEntity<Void> sessionDeleteToken(@PathVariable String token) {
    SessionDto session = sessionService.deleteSession(token);
    log.info("Deleted session={}", session);
    return ResponseEntity.noContent().build();
  }
}
