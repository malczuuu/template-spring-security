package pl.malczuuu.template.service.security;

import pl.malczuuu.template.service.common.error.NamedError;

public enum UserError implements NamedError {
  NOT_FOUND("User.NotFound"),
  ALREADY_DELETED("User.AlreadyDeleted"),
  PASSWORD_MISMATCH("User.PasswordMismatch"),
  DISABLED("User.Disabled"),
  EXPIRED("User.Expired"),
  UNAUTHORIZED("User.Unauthorized"),
  EMAIL_IN_USE("User.EmailInUse"),
  USERNAME_IN_USE("User.UsernameInUser"),
  ACCESS_DENIED("User.AccessDenied"),
  FORBIDDEN_FIELD_ROLES("User.ForbiddenFieldRoles"),
  SELF_DELETE_ATTEMPT("User.SelfDeleteAttempt");

  private final String name;

  UserError(String name) {
    this.name = name;
  }

  @Override
  public String getName() {
    return name;
  }
}
