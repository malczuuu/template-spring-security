package pl.malczuuu.template.service.security;

import java.io.Serializable;
import java.time.Instant;

public final class SessionDto implements Serializable {

  private static final long serialVersionUID = 1L;

  public static Builder builder() {
    return new Builder();
  }

  private final String token;
  private final UserDto user;
  private final Instant expiredAt;
  private final Instant createdAt;

  public SessionDto(String token, UserDto user, Instant expiredAt, Instant createdAt) {
    this.token = token;
    this.user = user;
    this.expiredAt = expiredAt;
    this.createdAt = createdAt;
  }

  public String getToken() {
    return token;
  }

  public UserDto getUser() {
    return user;
  }

  public Instant getExpiredAt() {
    return expiredAt;
  }

  public Instant getCreatedAt() {
    return createdAt;
  }

  public static class Builder {

    private String token;
    private UserDto user;
    private Instant expiredAt;
    private Instant createdAt;

    private Builder() {}

    public Builder token(String token) {
      this.token = token;
      return this;
    }

    public Builder user(UserDto user) {
      this.user = user;
      return this;
    }

    public Builder expiredAt(Instant expiredAt) {
      this.expiredAt = expiredAt;
      return this;
    }

    public Builder createdAt(Instant createdAt) {
      this.createdAt = createdAt;
      return this;
    }

    public SessionDto build() {
      return new SessionDto(token, user, expiredAt, createdAt);
    }
  }
}
