package pl.malczuuu.template.service.security;

import java.io.Serializable;
import java.time.Instant;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "sessions")
public class Session implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @Column(name = "session_id")
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "session_token", nullable = false, unique = true, updatable = false)
  private String token;

  @ManyToOne(targetEntity = User.class, optional = false)
  private User user;

  @Column(name = "session_deleted", nullable = false)
  private boolean deleted;

  @Column(name = "session_expired_at", nullable = false, updatable = false)
  private Instant expiredAt;

  @Column(name = "session_created_at", nullable = false, updatable = false)
  private Instant createdAt;

  // No-args constructor - for JPA
  protected Session() {}

  public Session(Long id, String token, User user, Instant expiredAt, Instant createdAt) {
    this.id = id;
    this.token = token;
    this.user = user;
    this.deleted = false;
    this.expiredAt = expiredAt;
    this.createdAt = createdAt;
  }

  public Session(String token, User user, Instant expiredAt) {
    this(null, token, user, expiredAt, Instant.now());
  }

  public Long getId() {
    return id;
  }

  public String getToken() {
    return token;
  }

  public User getUser() {
    return user;
  }

  public boolean isDeleted() {
    return deleted;
  }

  public Instant getExpiredAt() {
    return expiredAt;
  }

  public Instant getCreatedAt() {
    return createdAt;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public void setToken(String token) {
    this.token = token;
  }

  public void setUser(User user) {
    this.user = user;
  }

  public void setDeleted(boolean deleted) {
    this.deleted = deleted;
  }

  public void setExpiredAt(Instant expiredAt) {
    this.expiredAt = expiredAt;
  }

  public void setCreatedAt(Instant createdAt) {
    this.createdAt = createdAt;
  }
}
