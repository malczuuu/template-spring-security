package pl.malczuuu.template.service.security;

import pl.malczuuu.template.service.common.model.Mapper;

public class UserMapper implements Mapper<User, UserDto> {

  @Override
  public UserDto map(User user) {
    return UserDto.builder()
        .username(user.getUsername())
        .password(user.getPassword())
        .email(user.getEmail())
        .firstName(user.getFirstName())
        .lastName(user.getLastName())
        .publicEmail(user.getPublicEmail())
        .role(user.getRole())
        .deleted(user.isDeleted())
        .build();
  }
}
