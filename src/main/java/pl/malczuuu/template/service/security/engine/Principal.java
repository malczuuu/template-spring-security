package pl.malczuuu.template.service.security.engine;

import com.google.common.collect.ImmutableList;
import java.util.List;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import pl.malczuuu.template.service.security.Role;

/**
 * Defines authenticated user's basic details, which are sufficient to verify permissions.
 */
final class Principal implements UserDetails {

  private static final long serialVersionUID = 1L;

  private String username;
  private String password;
  private boolean accountNonExpired;
  private boolean accountNonLocked;
  private boolean credentialsNonExpired;
  private boolean enabled;
  private List<GrantedAuthority> authorities;

  Principal(
      String username,
      String password,
      boolean accountNonExpired,
      boolean accountNonLocked,
      boolean credentialsNonExpired,
      boolean enabled,
      List<GrantedAuthority> authorities) {
    this.username = username;
    this.password = password;
    this.accountNonExpired = accountNonExpired;
    this.accountNonLocked = accountNonLocked;
    this.credentialsNonExpired = credentialsNonExpired;
    this.enabled = enabled;
    this.authorities = ImmutableList.copyOf(authorities);
  }

  @Override
  public String getUsername() {
    return username;
  }

  @Override
  public String getPassword() {
    return password;
  }

  @Override
  public boolean isAccountNonExpired() {
    return accountNonExpired;
  }

  @Override
  public boolean isAccountNonLocked() {
    return accountNonLocked;
  }

  @Override
  public boolean isCredentialsNonExpired() {
    return credentialsNonExpired;
  }

  @Override
  public boolean isEnabled() {
    return enabled;
  }

  @Override
  public List<GrantedAuthority> getAuthorities() {
    return authorities;
  }
}
