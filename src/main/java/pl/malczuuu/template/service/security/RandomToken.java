package pl.malczuuu.template.service.security;

import java.util.Base64;
import org.apache.commons.lang3.RandomStringUtils;

public final class RandomToken {

  public static String sessionToken() {
    return RandomStringUtils.randomAlphanumeric(128);
  }

  public static String password() {
    return Base64.getEncoder().encodeToString(RandomStringUtils.randomAlphanumeric(10).getBytes());
  }

  private RandomToken() {}
}
