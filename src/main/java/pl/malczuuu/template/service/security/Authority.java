package pl.malczuuu.template.service.security;

import org.springframework.security.core.GrantedAuthority;

public enum Authority implements GrantedAuthority {
  ROOT,
  USER,
  ANONYMOUS;

  @Override
  public String getAuthority() {
    return name();
  }
}
