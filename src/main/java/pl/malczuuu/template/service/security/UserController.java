package pl.malczuuu.template.service.security;

import static java.util.stream.Collectors.toList;

import java.net.URI;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/api/users")
class UserController {

  private static final Logger log = LoggerFactory.getLogger(UserController.class);

  private final UserService userService;

  @Autowired
  UserController(UserService userService) {
    this.userService = userService;
  }

  @PreAuthorize("hasAuthority('ROOT') or hasAuthority('USER')")
  @RequestMapping(method = RequestMethod.GET, produces = "application/json")
  public ResponseEntity<Page<UserDto>> userGet(
      Pageable pageable, @AuthenticationPrincipal UserDetails principal) {
    Page<UserDto> pageOfUsers = userService.getPageOfUsers(pageable);
    log.info(
        "Retrieved count={} of users by principal={}, data={}",
        pageOfUsers.getNumberOfElements(),
        principal.getUsername(),
        pageOfUsers.stream().map(u -> u.getUsername()).collect(toList()));
    return ResponseEntity.ok(pageOfUsers);
  }

  @PreAuthorize("hasAuthority('ROOT')")
  @RequestMapping(
    method = RequestMethod.POST,
    produces = "application/json",
    consumes = "application/json"
  )
  public ResponseEntity<UserDto> userPost(
      @RequestBody @Valid UserToCreateDto dto, @AuthenticationPrincipal UserDetails principal) {
    UserDto user = userService.createUser(dto);
    log.info("Created user={} by principal={}", user.getUsername(), principal.getUsername());
    return ResponseEntity.created(URI.create(String.format("/api/users/%s", user.getUsername())))
        .body(user);
  }

  @PreAuthorize("hasAuthority('ROOT') or hasAuthority('USER')")
  @RequestMapping(path = "/{username}", method = RequestMethod.GET, produces = "application/json")
  public ResponseEntity<UserDto> userGetUsername(
      @PathVariable String username, @AuthenticationPrincipal UserDetails principal) {
    UserDto user = userService.getUser(username);
    log.info("Retrieved user={} by principal={}", user.getUsername(), principal.getUsername());
    return ResponseEntity.ok(user);
  }

  @PreAuthorize("hasAuthority('ROOT') or hasAuthority('USER')")
  @RequestMapping(
    path = "/{username}",
    method = RequestMethod.PUT,
    produces = "application/json",
    consumes = "application/json"
  )
  public ResponseEntity<UserDto> userPutUsername(
      @PathVariable String username,
      @RequestBody @Valid UserDetailsDto dto,
      @AuthenticationPrincipal UserDetails principal) {
    UserDto user = userService.updateUser(username, dto, principal);
    log.info(
        "Updated user={} by principal={}, data={}",
        user.getUsername(),
        principal.getUsername(),
        user);
    return ResponseEntity.ok(user);
  }

  @PreAuthorize("hasAuthority('ROOT')")
  @RequestMapping(path = "/{username}", method = RequestMethod.DELETE)
  public ResponseEntity<Void> userDeleteUsername(
      @PathVariable String username, @AuthenticationPrincipal UserDetails principal) {
    UserDto user = userService.deleteUser(username, principal);
    log.info("Deleted user={} by principal={}", user.getUsername(), principal.getUsername());
    return ResponseEntity.noContent().build();
  }
}
