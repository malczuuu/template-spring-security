package pl.malczuuu.template.service.security;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetails;
import pl.malczuuu.template.service.common.error.BadRequestException;
import pl.malczuuu.template.service.common.error.GoneException;
import pl.malczuuu.template.service.common.error.NotFoundException;
import pl.malczuuu.template.service.common.error.UnauthorizedException;

public interface UserService {

  Page<UserDto> getPageOfUsers(Pageable pageable);

  UserDto getUser(String username) throws NotFoundException, GoneException;

  UserDto getUser(String username, String password)
      throws UnauthorizedException, NotFoundException, GoneException;

  UserDto createUser(UserToCreateDto dto);

  UserDto updateUser(String username, UserDetailsDto dto, UserDetails principal)
      throws NotFoundException, GoneException;

  UserDto deleteUser(String username, UserDetails principal)
      throws NotFoundException, GoneException, BadRequestException;

  long getCount();
}
