package pl.malczuuu.template.service.security;

import java.time.Instant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.malczuuu.template.service.common.error.UnauthorizedException;

@Service
class SessionServiceImpl implements SessionService {

  private final SessionMapper sessionMapper = new SessionMapper();
  private final UserRepository userRepository;
  private final SessionRepository sessionRepository;

  @Autowired
  SessionServiceImpl(UserRepository userRepository, SessionRepository sessionRepository) {
    this.userRepository = userRepository;
    this.sessionRepository = sessionRepository;
  }

  @Override
  @Transactional
  public SessionDto createSession(CredentialsDto dto) throws UnauthorizedException {
    Instant expiredAt =
        dto.isRemembered()
            ? Instant.now().plusSeconds(60 * 60 * 24 * 30)
            : Instant.now().plusSeconds(60 * 60 * 24 * 7);
    return sessionMapper.map(
        sessionRepository.save(
            new Session(
                RandomToken.sessionToken(),
                userRepository
                    .findByUsername(dto.getUsername())
                    .filter(u -> BCrypt.checkpw(dto.getPassword(), u.getPassword()))
                    .orElseThrow(() -> new UnauthorizedException(SessionError.UNAUTHORIZED)),
                expiredAt)));
  }

  @Override
  @Transactional
  public SessionDto getSession(String token) throws UnauthorizedException {
    return sessionMapper.map(
        sessionRepository
            .findByTokenAndDeleted(token, false)
            .filter(s -> s.getExpiredAt().isAfter(Instant.now()))
            .orElseThrow(() -> new UnauthorizedException(SessionError.NOT_FOUND)));
  }

  @Override
  @Transactional
  public SessionDto deleteSession(String token) throws UnauthorizedException {
    Session session =
        sessionRepository
            .findByTokenAndDeleted(token, false)
            .orElseThrow(() -> new UnauthorizedException(SessionError.NOT_FOUND));
    session.setDeleted(true);
    session = sessionRepository.save(session);
    return sessionMapper.map(session);
  }
}
