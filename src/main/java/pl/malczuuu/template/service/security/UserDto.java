package pl.malczuuu.template.service.security;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;

@JsonIgnoreProperties({"password", "deleted"})
public final class UserDto implements Serializable {

  private static final long serialVersionUID = 1L;

  public static Builder builder() {
    return new Builder();
  }

  private final String username;
  private final String password;
  private final String email;
  private final String firstName;
  private final String lastName;
  private final String publicEmail;
  private final Role role;
  private final boolean deleted;

  public UserDto(
      String username,
      String password,
      String email,
      String firstName,
      String lastName,
      String publicEmail,
      Role role,
      boolean deleted) {
    this.username = username;
    this.password = password;
    this.email = email;
    this.firstName = firstName;
    this.lastName = lastName;
    this.publicEmail = publicEmail;
    this.role = role;
    this.deleted = deleted;
  }

  public String getUsername() {
    return username;
  }

  public String getPassword() {
    return password;
  }

  public String getEmail() {
    return email;
  }

  public String getFirstName() {
    return firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public String getPublicEmail() {
    return publicEmail;
  }

  public Role getRole() {
    return role;
  }

  public boolean isDeleted() {
    return deleted;
  }

  public static class Builder {

    private Long uid;
    private String username;
    private String password;
    private String email;
    private String firstName;
    private String lastName;
    private String publicEmail;
    private Role role;
    private boolean deleted;

    private Builder() {}

    public Builder username(String username) {
      this.username = username;
      return this;
    }

    public Builder password(String password) {
      this.password = password;
      return this;
    }

    public Builder email(String email) {
      this.email = email;
      return this;
    }

    public Builder firstName(String firstName) {
      this.firstName = firstName;
      return this;
    }

    public Builder lastName(String lastName) {
      this.lastName = lastName;
      return this;
    }

    public Builder publicEmail(String publicEmail) {
      this.publicEmail = publicEmail;
      return this;
    }

    public Builder role(Role role) {
      this.role = role;
      return this;
    }

    public Builder deleted(boolean deleted) {
      this.deleted = deleted;
      return this;
    }

    public UserDto build() {
      return new UserDto(
          username, password, email, firstName, lastName, publicEmail, role, deleted);
    }
  }
}
