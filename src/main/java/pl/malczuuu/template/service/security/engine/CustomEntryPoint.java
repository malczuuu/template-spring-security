package pl.malczuuu.template.service.security.engine;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;
import pl.malczuuu.template.service.common.error.ErrorDto;

/**
 * Exception handler for Spring Security filters. Enhances response with correct status code and
 * response body.
 */
@Component
class CustomEntryPoint implements AuthenticationEntryPoint {

  private final ObjectMapper mapper;

  @Autowired
  CustomEntryPoint(ObjectMapper mapper) {
    this.mapper = mapper;
  }

  @Override
  public void commence(
      HttpServletRequest request,
      HttpServletResponse response,
      AuthenticationException authenticationException)
      throws IOException {
    response.setContentType("application/json");
    response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
    ErrorDto error =
        ErrorDto.builder()
            .code(HttpStatus.UNAUTHORIZED.value())
            .message(authenticationException.getMessage())
            .build();
    response.getOutputStream().println(mapper.writeValueAsString(error));
  }
}
