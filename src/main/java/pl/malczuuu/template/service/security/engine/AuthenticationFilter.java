package pl.malczuuu.template.service.security.engine;

import java.io.IOException;
import java.util.Base64;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.util.Strings;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

@Component
class AuthenticationFilter extends OncePerRequestFilter {

  /**
   * Decomposes HTTP Authorization header into understandable form (Token based authentication,
   * Basic authentication or Anonymous authentication).
   *
   * @param request Servlet HTTP request
   * @param response Servlet HTTP response
   * @param filterChain invocation of changes to request
   * @throws ServletException inherited from filterChain.doFilter
   * @throws IOException inherited from filterChain.doFilter
   */
  @Override
  protected void doFilterInternal(
      HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
      throws ServletException, IOException {
    String authorization = request.getHeader("Authorization");
    if (Strings.isNotBlank(authorization)) {
      String[] decomposed = authorization.split(" ");
      if (decomposed.length == 2) {
        if (decomposed[0].equalsIgnoreCase("Basic")) {
          decomposed[1] = new String(Base64.getDecoder().decode(decomposed[1]));
        }
        SecurityContextHolder.getContext()
            .setAuthentication(new CredentialsToken(decomposed[0], decomposed[1]));
      }
    } else {
      SecurityContextHolder.getContext().setAuthentication(new CredentialsToken("Anonymous", ""));
    }
    filterChain.doFilter(request, response);
  }
}
