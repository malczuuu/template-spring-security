package pl.malczuuu.template.service.security.engine;

import java.util.Collections;
import org.springframework.security.authentication.AbstractAuthenticationToken;

/**
 * Class which objects are used to pass decomposed Authorization header forward to the next
 * Authorization service.
 */
class CredentialsToken extends AbstractAuthenticationToken {

  private static final long serialVersionUID = 1L;

  /**
   * In current implementation principal is treated as the authentication method name (like Basic,
   * Token or Anonymous).
   */
  private final String principal;

  /**
   * Contains authentication token or credentials, depending on used authentication method.
   */
  private final String credentials;

  CredentialsToken(String principal, String credentials) {
    super(Collections.emptyList());
    this.principal = principal;
    this.credentials = credentials;
  }

  /**
   * Returns authentication method name.
   *
   * @return authentication method name
   */
  public String getPrincipal() {
    return principal;
  }

  /**
   * Returns authentication token or credentials.
   *
   * @return authentication token or credentials
   */
  public String getCredentials() {
    return credentials;
  }
}
