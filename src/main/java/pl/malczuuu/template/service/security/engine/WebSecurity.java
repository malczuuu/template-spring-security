package pl.malczuuu.template.service.security.engine;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

/**
 * Main Web-Security configuration class. Binds whole native Spring Security mechanisms with custom
 * implemented components.
 *
 * @see AuthenticationFilter
 * @see CredentialsToken
 * @see CustomEntryPoint
 * @see CustomProvider
 * @see Principal
 * @see PrincipalService
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
class WebSecurity extends WebSecurityConfigurerAdapter {

  private final AuthenticationFilter authenticationFilter;
  private final CustomEntryPoint customEntryPoint;
  private final CustomProvider customProvider;

  @Autowired
  WebSecurity(
      AuthenticationFilter authenticationFilter,
      CustomEntryPoint customEntryPoint,
      CustomProvider customProvider) {
    this.authenticationFilter = authenticationFilter;
    this.customEntryPoint = customEntryPoint;
    this.customProvider = customProvider;
  }

  @Override
  protected void configure(AuthenticationManagerBuilder auth) {
    auth.authenticationProvider(customProvider);
  }

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http.cors()
        .and()
        .csrf()
        .disable()
        .sessionManagement()
        .sessionCreationPolicy(SessionCreationPolicy.NEVER)
        .and()
        .authorizeRequests()
        .anyRequest()
        .permitAll()
        .and()
        .addFilterBefore(authenticationFilter, BasicAuthenticationFilter.class)
        .exceptionHandling()
        .authenticationEntryPoint(customEntryPoint);
  }
}
