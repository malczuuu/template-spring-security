package pl.malczuuu.template.service.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.malczuuu.template.service.common.error.BadRequestException;
import pl.malczuuu.template.service.common.error.ConflictException;
import pl.malczuuu.template.service.common.error.GoneException;
import pl.malczuuu.template.service.common.error.NotFoundException;
import pl.malczuuu.template.service.common.error.UnauthorizedException;

@Service
class UserServiceImpl implements UserService {

  private final UserMapper userMapper = new UserMapper();
  private final UserRepository userRepository;

  @Autowired
  UserServiceImpl(UserRepository userRepository) {
    this.userRepository = userRepository;
  }

  @Override
  @Transactional
  public Page<UserDto> getPageOfUsers(Pageable pageable) {
    return userRepository.findAllByDeleted(false, pageable).map(user -> userMapper.map(user));
  }

  @Override
  @Transactional
  public UserDto getUser(String username) throws NotFoundException, GoneException {
    return userMapper.map(fetchUser(username));
  }

  @Override
  @Transactional
  public UserDto getUser(String username, String password)
      throws NotFoundException, GoneException, UnauthorizedException {
    User user = fetchUser(username);
    if (!BCrypt.checkpw(password, user.getPassword())) {
      throw new UnauthorizedException(UserError.PASSWORD_MISMATCH);
    }
    return userMapper.map(user);
  }

  private User fetchUser(String username) throws NotFoundException, GoneException {
    User user =
        userRepository
            .findByUsername(username)
            .orElseThrow(() -> new NotFoundException(UserError.NOT_FOUND));
    if (user.isDeleted()) {
      throw new GoneException(UserError.ALREADY_DELETED);
    }
    return user;
  }

  @Override
  @Transactional
  public UserDto createUser(UserToCreateDto dto) {
    validateUniqueUsername(dto.getUsername());
    validateUniqueEmail(dto.getEmail());
    return userMapper.map(
        userRepository.save(
            new User(
                dto.getUsername(),
                BCrypt.hashpw(dto.getPassword(), BCrypt.gensalt()),
                dto.getEmail(),
                dto.getFirstName(),
                dto.getLastName(),
                "",
                dto.getRole())));
  }

  private void validateUniqueUsername(String username) {
    if (userRepository.existsByUsername(username)) {
      throw new ConflictException(UserError.USERNAME_IN_USE);
    }
  }

  private void validateUniqueEmail(String email) {
    if (userRepository.existsByEmail(email)) {
      throw new ConflictException(UserError.EMAIL_IN_USE);
    }
  }

  @Override
  @Transactional
  public UserDto updateUser(String username, UserDetailsDto dto, UserDetails principal)
      throws NotFoundException, GoneException {
    User user = fetchUser(username);
    if (principal.getAuthorities().stream().anyMatch(a -> a == Authority.ROOT)) {
      return updateUserAsAdmin(user, dto);
    } else if (principal.getUsername().equals(user.getUsername())) {
      return updateSelfUser(user, dto);
    } else {
      throw new UnauthorizedException(UserError.ACCESS_DENIED);
    }
  }

  private UserDto updateUserAsAdmin(User user, UserDetailsDto dto) {
    validateUniqueEmail(dto.getEmail());
    user.setEmail(dto.getEmail());
    user.setFirstName(dto.getFirstName());
    user.setLastName(dto.getLastName());
    user.setPublicEmail(dto.getPublicEmail());
    user.setRoles(dto.getRole());
    return userMapper.map(userRepository.save(user));
  }

  private UserDto updateSelfUser(User user, UserDetailsDto dto) {
    if (dto.getRole() != user.getRole()) {
      throw new BadRequestException(UserError.FORBIDDEN_FIELD_ROLES);
    }
    validateUniqueEmail(dto.getEmail());
    user.setEmail(dto.getEmail());
    user.setFirstName(dto.getFirstName());
    user.setLastName(dto.getLastName());
    user.setPublicEmail(dto.getPublicEmail());
    return userMapper.map(userRepository.save(user));
  }

  @Override
  @Transactional
  public UserDto deleteUser(String username, UserDetails principal)
      throws NotFoundException, GoneException, BadRequestException {
    User user = fetchUser(username);
    if (user.isDeleted()) {
      throw new BadRequestException(UserError.ALREADY_DELETED);
    }
    if (user.getUsername().equals(principal.getUsername())) {
      throw new BadRequestException(UserError.SELF_DELETE_ATTEMPT);
    }
    user.setDeleted(true);
    user = userRepository.save(user);
    return userMapper.map(user);
  }

  @Override
  public long getCount() {
    return userRepository.count();
  }
}
