package pl.malczuuu.template.service.security;

import com.google.common.collect.ImmutableList;
import java.util.List;
import org.springframework.security.core.GrantedAuthority;

public enum Role {
  ADMIN(Authority.ROOT, Authority.USER, Authority.ANONYMOUS),
  USER(Authority.USER, Authority.ANONYMOUS);

  private final List<GrantedAuthority> authorities;

  Role(GrantedAuthority... authorities) {
    this.authorities = ImmutableList.copyOf(authorities);
  }

  public List<GrantedAuthority> getAuthorities() {
    return authorities;
  }
}
