package pl.malczuuu.template.service.security;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;

public final class UserToCreateDto implements Serializable {

  private static final long serialVersionUID = 1L;

  public static Builder builder() {
    return new Builder();
  }

  private final String username;
  private final String password;
  private final String firstName;
  private final String lastName;
  private final String email;
  private final Role role;

  @JsonCreator
  public UserToCreateDto(
      @JsonProperty("username") String username,
      @JsonProperty("password") String password,
      @JsonProperty("firstName") String firstName,
      @JsonProperty("lastName") String lastName,
      @JsonProperty("email") String email,
      @JsonProperty("role") Role role) {
    this.username = username;
    this.password = password;
    this.firstName = firstName;
    this.lastName = lastName;
    this.email = email;
    this.role = role;
  }

  public String getUsername() {
    return username;
  }

  public String getPassword() {
    return password;
  }

  public String getFirstName() {
    return firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public String getEmail() {
    return email;
  }

  public Role getRole() {
    return role;
  }

  public static class Builder {

    private String username;
    private String password;
    private String firstName;
    private String lastName;
    private String email;
    private Role roles;

    private Builder() {}

    public Builder username(String username) {
      this.username = username;
      return this;
    }

    public Builder password(String password) {
      this.password = password;
      return this;
    }

    public Builder firstName(String firstName) {
      this.firstName = firstName;
      return this;
    }

    public Builder lastName(String lastName) {
      this.lastName = lastName;
      return this;
    }

    public Builder email(String email) {
      this.email = email;
      return this;
    }

    public Builder role(Role roles) {
      this.roles = roles;
      return this;
    }

    public UserToCreateDto build() {
      return new UserToCreateDto(username, password, firstName, lastName, email, roles);
    }
  }
}
