package pl.malczuuu.template.service.security;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;

public final class CredentialsDto implements Serializable {

  private static final long serialVersionUID = 1L;

  public static Builder builder() {
    return new Builder();
  }

  private final String username;
  private final String password;
  private final boolean remembered;

  @JsonCreator
  public CredentialsDto(
      @JsonProperty("username") String username,
      @JsonProperty("password") String password,
      @JsonProperty("remembered") boolean remembered) {
    this.username = username;
    this.password = password;
    this.remembered = remembered;
  }

  public String getUsername() {
    return username;
  }

  public String getPassword() {
    return password;
  }

  public boolean isRemembered() {
    return remembered;
  }

  public static class Builder {

    private String username;
    private String password;
    private boolean remembered;

    private Builder() {}

    public Builder username(String username) {
      this.username = username;
      return this;
    }

    public Builder password(String password) {
      this.password = password;
      return this;
    }

    public Builder remembered(boolean remembered) {
      this.remembered = remembered;
      return this;
    }

    public CredentialsDto build() {
      return new CredentialsDto(username, password, remembered);
    }
  }
}
