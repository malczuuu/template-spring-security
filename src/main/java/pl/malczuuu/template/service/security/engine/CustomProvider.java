package pl.malczuuu.template.service.security.engine;

import com.google.common.collect.ImmutableList;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AccountExpiredException;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.CredentialsExpiredException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.authentication.ProviderNotFoundException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import pl.malczuuu.template.service.common.error.ServiceException;
import pl.malczuuu.template.service.security.Authority;
import pl.malczuuu.template.service.security.SessionError;
import pl.malczuuu.template.service.security.SessionService;
import pl.malczuuu.template.service.security.UserDto;
import pl.malczuuu.template.service.security.UserError;
import pl.malczuuu.template.service.security.UserService;

/**
 * Main authentication engine used with current Spring Security implementation. Filters requested
 * principal's permissions.
 */
@Slf4j
@Component
class CustomProvider implements AuthenticationProvider {

  private final UserService userService;
  private final SessionService sessionService;

  @Autowired
  CustomProvider(UserService userService, SessionService sessionService) {
    this.userService = userService;
    this.sessionService = sessionService;
  }

  @Override
  public Authentication authenticate(Authentication authentication) throws AuthenticationException {
    String method = ((CredentialsToken) authentication).getPrincipal();
    String token = ((CredentialsToken) authentication).getCredentials();
    if (method.equalsIgnoreCase("Token")) {
      return tokenAuthentication(token);
    } else if (method.equalsIgnoreCase("Basic")) {
      return basicAuthentication(token);
    } else if (method.equalsIgnoreCase("Anonymous")) {
      return anonymousAuthentication();
    } else {
      throw new ProviderNotFoundException("Unknown authentication method");
    }
  }

  private Authentication basicAuthentication(String credentials) throws ServiceException {
    try {
      String[] decomposed = credentials.split(":");
      String name = decomposed[0];
      String password = decomposed[1];
      UserDto user = userService.getUser(name, password);
      Principal principal =
          new Principal(
              user.getUsername(),
              user.getPassword(),
              true,
              true,
              true,
              true,
              ImmutableList.copyOf(user.getRole().getAuthorities()));
      validatePrincipal(principal);
      return new UsernamePasswordAuthenticationToken(
          principal, password, principal.getAuthorities());
    } catch (ServiceException e) {
      throw new UsernameNotFoundException(SessionError.UNAUTHORIZED.getName());
    }
  }

  /**
   * Token authentication provider.
   *
   * @param token requested user's session token
   * @return authenticated principal object
   */
  private Authentication tokenAuthentication(String token) {
    try {
      UserDto user = sessionService.getSession(token).getUser();
      Principal principal =
          new Principal(
              user.getUsername(),
              user.getPassword(),
              true,
              true,
              true,
              true,
              ImmutableList.copyOf(user.getRole().getAuthorities()));
      validatePrincipal(principal);
      return new UsernamePasswordAuthenticationToken(
          principal, token, ImmutableList.copyOf(principal.getAuthorities()));
    } catch (ServiceException e) {
      throw new UsernameNotFoundException(SessionError.UNAUTHORIZED.getName());
    }
  }

  /**
   * Anonymous authentication provider.
   *
   * @return authenticated principal object
   */
  private Authentication anonymousAuthentication() {
    return new UsernamePasswordAuthenticationToken(
        new Principal(
            "anonymous", "", true, true, true, true, ImmutableList.of(Authority.ANONYMOUS)),
        "",
        ImmutableList.of(Authority.ANONYMOUS));
  }

  /**
   * Validates if authenticated principal is allowed to pass authorization.
   *
   * @param principal authenticated user's details
   */
  private void validatePrincipal(UserDetails principal) {
    if (!principal.isEnabled()) {
      throw new DisabledException(UserError.DISABLED.getName());
    }
    if (!principal.isCredentialsNonExpired()) {
      throw new CredentialsExpiredException(UserError.DISABLED.getName());
    }
    if (!principal.isAccountNonExpired()) {
      throw new AccountExpiredException(UserError.EXPIRED.getName());
    }
    if (!principal.isAccountNonLocked()) {
      throw new LockedException(UserError.DISABLED.getName());
    }
  }

  @Override
  public boolean supports(Class<?> authentication) {
    return CredentialsToken.class.isAssignableFrom(authentication);
  }
}
