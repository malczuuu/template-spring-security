package pl.malczuuu.template.service.security;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "users")
public class User implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @Column(name = "user_id")
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "user_username", unique = true, nullable = false, updatable = false)
  private String username;

  @Column(name = "user_password", nullable = false)
  private String password;

  @Column(name = "user_email", unique = true, nullable = false)
  private String email;

  @Column(name = "user_first_name", nullable = false)
  private String firstName;

  @Column(name = "user_last_name", nullable = false)
  private String lastName;

  @Column(name = "user_public_email", nullable = false)
  private String publicEmail;

  @Column(name = "user_role", nullable = false, updatable = false)
  @Enumerated(EnumType.STRING)
  private Role role;

  @Column(name = "user_deleted", nullable = false)
  private boolean deleted;

  // No-args constructor - for JPA
  protected User() {
  }

  public User(
      Long id,
      String username,
      String password,
      String email,
      String firstName,
      String lastName,
      String publicEmail,
      Role role,
      boolean deleted) {
    this.id = id;
    this.username = username;
    this.password = password;
    this.email = email;
    this.firstName = firstName;
    this.lastName = lastName;
    this.publicEmail = publicEmail;
    this.role = role;
    this.deleted = deleted;
  }

  public User(
      String username,
      String password,
      String email,
      String firstName,
      String lastName,
      String publicEmail,
      Role role) {
    this(null, username, password, email, firstName, lastName, publicEmail, role, false);
  }

  public Long getId() {
    return id;
  }

  public String getUsername() {
    return username;
  }

  public String getPassword() {
    return password;
  }

  public String getEmail() {
    return email;
  }

  public String getFirstName() {
    return firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public String getPublicEmail() {
    return publicEmail;
  }

  public Role getRole() {
    return role;
  }

  public boolean isDeleted() {
    return deleted;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public void setPublicEmail(String publicEmail) {
    this.publicEmail = publicEmail;
  }

  public void setRoles(Role role) {
    this.role = role;
  }

  public void setDeleted(boolean deleted) {
    this.deleted = deleted;
  }
}
