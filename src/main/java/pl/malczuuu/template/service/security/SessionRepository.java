package pl.malczuuu.template.service.security;

import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SessionRepository extends JpaRepository<Session, Long> {

  Optional<Session> findByTokenAndDeleted(String token, boolean deleted);
}
