package pl.malczuuu.template.service.security;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;

public final class UserDetailsDto implements Serializable {

  private static final long serialVersionUID = 1L;

  public static Builder builder() {
    return new Builder();
  }

  private final String email;
  private final String firstName;
  private final String lastName;
  private final String publicEmail;
  private final Role role;

  @JsonCreator
  public UserDetailsDto(
      @JsonProperty("email") String email,
      @JsonProperty("firstName") String firstName,
      @JsonProperty("lastName") String lastName,
      @JsonProperty("publicEmail") String publicEmail,
      @JsonProperty("role") Role role) {
    this.email = email;
    this.firstName = firstName;
    this.lastName = lastName;
    this.publicEmail = publicEmail;
    this.role = role;
  }

  public String getEmail() {
    return email;
  }

  public String getFirstName() {
    return firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public String getPublicEmail() {
    return publicEmail;
  }

  public Role getRole() {
    return role;
  }

  public static class Builder {

    private String email;
    private String firstName;
    private String lastName;
    private String publicEmail;
    private Role role;

    private Builder() {
    }

    public Builder email(String email) {
      this.email = email;
      return this;
    }

    public Builder firstName(String firstName) {
      this.firstName = firstName;
      return this;
    }

    public Builder lastName(String lastName) {
      this.lastName = lastName;
      return this;
    }

    public Builder publicEmail(String publicEmail) {
      this.publicEmail = publicEmail;
      return this;
    }

    public Builder role(Role role) {
      this.role = role;
      return this;
    }

    public UserDetailsDto build() {
      return new UserDetailsDto(email, firstName, lastName, publicEmail, role);
    }
  }
}
