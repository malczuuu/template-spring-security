package pl.malczuuu.template.service.setup;

import com.google.common.collect.ImmutableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import pl.malczuuu.template.service.security.Role;
import pl.malczuuu.template.service.security.UserService;
import pl.malczuuu.template.service.security.UserToCreateDto;

@Component
class AdminInitializer implements CommandLineRunner {

  private final UserService userService;

  @Autowired
  AdminInitializer(UserService userService) {
    this.userService = userService;
  }

  @Override
  public void run(String... args) throws Exception {
    if (userService.getCount() == 0) {
      userService.createUser(
          UserToCreateDto.builder()
              .username("root")
              .password("root")
              .email("root@example.com")
              .firstName("")
              .lastName("")
              .role(Role.ADMIN)
              .build());
    }
  }
}
