package pl.malczuuu.template.service.common.error;

public class ConflictException extends ServiceException {

  private static final long serialVersionUID = 1L;

  public ConflictException(NamedError error) {
    super(409, error.getName());
  }
}
