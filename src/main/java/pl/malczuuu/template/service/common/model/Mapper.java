package pl.malczuuu.template.service.common.model;

@FunctionalInterface
public interface Mapper<IN, OUT> {

  OUT map(IN in);
}
