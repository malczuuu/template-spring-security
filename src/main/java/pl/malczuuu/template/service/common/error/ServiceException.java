package pl.malczuuu.template.service.common.error;

import lombok.Getter;

@Getter
public abstract class ServiceException extends RuntimeException {

  private static final long serialVersionUID = 1L;

  private final int status;

  ServiceException(int status, String message) {
    super(message);
    this.status = status;
  }
}
