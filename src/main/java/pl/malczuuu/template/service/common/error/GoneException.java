package pl.malczuuu.template.service.common.error;

public class GoneException extends ServiceException {

  private static final long serialVersionUID = 1L;

  public GoneException(NamedError error) {
    super(410, error.getName());
  }
}
