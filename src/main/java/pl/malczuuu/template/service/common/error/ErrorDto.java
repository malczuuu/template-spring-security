package pl.malczuuu.template.service.common.error;

import java.io.Serializable;
import java.time.Clock;
import java.time.LocalDateTime;

public final class ErrorDto implements Serializable {

  private static final long serialVersionUID = 1L;

  public static Builder builder() {
    return new Builder();
  }

  private final Integer code;
  private final String message;
  private final LocalDateTime timestamp = LocalDateTime.now(Clock.systemUTC());

  public ErrorDto(Integer code, String message) {
    this.code = code;
    this.message = message;
  }

  public Integer getCode() {
    return code;
  }

  public String getMessage() {
    return message;
  }

  public LocalDateTime getTimestamp() {
    return timestamp;
  }

  public static class Builder {

    private Integer code;
    private String message;

    private Builder() {}

    public Builder code(Integer code) {
      this.code = code;
      return this;
    }

    public Builder message(String message) {
      this.message = message;
      return this;
    }

    public ErrorDto build() {
      return new ErrorDto(code, message);
    }
  }
}
