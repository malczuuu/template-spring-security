package pl.malczuuu.template.service.common.error;

public class UnauthorizedException extends ServiceException {

  private static final long serialVersionUID = 1L;

  public UnauthorizedException(NamedError error) {
    super(401, error.getName());
  }
}
