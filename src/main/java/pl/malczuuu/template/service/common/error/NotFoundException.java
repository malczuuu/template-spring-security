package pl.malczuuu.template.service.common.error;

public class NotFoundException extends ServiceException {

  private static final long serialVersionUID = 1L;

  public NotFoundException(NamedError error) {
    super(404, error.getName());
  }
}
