package pl.malczuuu.template.service.common.error;

public class BadRequestException extends ServiceException {

  private static final long serialVersionUID = 1L;

  public BadRequestException(NamedError error) {
    super(400, error.getName());
  }
}
