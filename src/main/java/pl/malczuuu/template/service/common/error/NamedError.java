package pl.malczuuu.template.service.common.error;

import java.io.Serializable;

public interface NamedError extends Serializable {

  String getName();
}
