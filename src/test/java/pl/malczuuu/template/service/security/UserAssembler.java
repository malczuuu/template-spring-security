package pl.malczuuu.template.service.security;

import java.util.concurrent.ThreadLocalRandom;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.security.crypto.bcrypt.BCrypt;

public class UserAssembler {

  public static User admin() {
    String username = RandomStringUtils.randomAlphabetic(10);
    return new User(
        ThreadLocalRandom.current().nextLong(),
        username,
        BCrypt.hashpw(RandomStringUtils.randomAlphabetic(10), BCrypt.gensalt()),
        username + "@example.com",
        RandomStringUtils.randomAlphabetic(10),
        RandomStringUtils.randomAlphabetic(10),
        username + "@example.com",
        Role.ADMIN,
        false);
  }

  public static User random() {
    String username = RandomStringUtils.randomAlphabetic(10);
    return new User(
        ThreadLocalRandom.current().nextLong(),
        username,
        BCrypt.hashpw(RandomStringUtils.randomAlphabetic(10), BCrypt.gensalt()),
        username + "@example.com",
        RandomStringUtils.randomAlphabetic(10),
        RandomStringUtils.randomAlphabetic(10),
        username + "@example.com",
        Role.USER,
        false);
  }
}
