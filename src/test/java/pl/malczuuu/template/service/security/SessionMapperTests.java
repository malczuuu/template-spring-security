package pl.malczuuu.template.service.security;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class SessionMapperTests {

  private final SessionMapper sessionMapper = new SessionMapper();

  @Test
  public void mapTest() {
    Session session = SessionAssembler.root();
    SessionDto dto = sessionMapper.map(session);
    assertEquals(session.getToken(), dto.getToken());
    assertEquals(session.getUser().getUsername(), dto.getUser().getUsername());
    assertEquals(session.getExpiredAt(), dto.getExpiredAt());
    assertEquals(session.getCreatedAt(), dto.getCreatedAt());
  }
}
