package pl.malczuuu.template.service.security.engine;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.google.common.collect.ImmutableList;
import org.junit.Test;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import pl.malczuuu.template.service.common.error.GoneException;
import pl.malczuuu.template.service.common.error.NotFoundException;
import pl.malczuuu.template.service.common.error.UnauthorizedException;
import pl.malczuuu.template.service.security.Authority;
import pl.malczuuu.template.service.security.SessionAssembler;
import pl.malczuuu.template.service.security.SessionDto;
import pl.malczuuu.template.service.security.SessionError;
import pl.malczuuu.template.service.security.SessionMapper;
import pl.malczuuu.template.service.security.SessionService;
import pl.malczuuu.template.service.security.UserAssembler;
import pl.malczuuu.template.service.security.UserDto;
import pl.malczuuu.template.service.security.UserError;
import pl.malczuuu.template.service.security.UserMapper;
import pl.malczuuu.template.service.security.UserService;

public class CustomProviderTests {

  private UserService userService = mock(UserService.class);
  private SessionService sessionService = mock(SessionService.class);
  private CustomProvider customProvider = new CustomProvider(userService, sessionService);

  @Test
  public void authenticateTokenTest() {
    SessionDto session = new SessionMapper().map(SessionAssembler.root());
    when(sessionService.getSession(anyString())).thenReturn(session);

    Authentication authentication =
        customProvider.authenticate(new CredentialsToken("Token", session.getToken()));

    assertTrue(authentication instanceof UsernamePasswordAuthenticationToken);
    UsernamePasswordAuthenticationToken casted =
        (UsernamePasswordAuthenticationToken) authentication;
    assertEquals(
        session.getUser().getUsername(), ((UserDetails) casted.getPrincipal()).getUsername());
    assertEquals(session.getToken(), casted.getCredentials());
    assertEquals(session.getUser().getRole().getAuthorities(), authentication.getAuthorities());
  }

  @Test(expected = UsernameNotFoundException.class)
  public void authenticateTokenTest_Unauthorized() {
    SessionDto session = new SessionMapper().map(SessionAssembler.root());
    when(sessionService.getSession(anyString()))
        .thenThrow(new UnauthorizedException(SessionError.UNAUTHORIZED));

    customProvider.authenticate(new CredentialsToken("Token", session.getToken()));
  }

  @Test
  public void authenticateBasicTest() {
    UserDto user = new UserMapper().map(UserAssembler.random());
    when(userService.getUser(anyString(), anyString())).thenReturn(user);

    Authentication authentication =
        customProvider.authenticate(
            new CredentialsToken("Basic", user.getUsername() + ":password"));

    assertTrue(authentication instanceof UsernamePasswordAuthenticationToken);
    UsernamePasswordAuthenticationToken casted =
        (UsernamePasswordAuthenticationToken) authentication;
    assertEquals(user.getUsername(), ((UserDetails) casted.getPrincipal()).getUsername());
    assertEquals("password", casted.getCredentials());
    assertEquals(user.getRole().getAuthorities(), authentication.getAuthorities());
  }

  @Test(expected = UsernameNotFoundException.class)
  public void authenticateBasicTest_NotFound() {
    when(userService.getUser(anyString(), anyString()))
        .thenThrow(new NotFoundException(UserError.NOT_FOUND));

    customProvider.authenticate(new CredentialsToken("Basic", "username:password"));
  }

  @Test(expected = UsernameNotFoundException.class)
  public void authenticateBasicTest_Gone() {
    when(userService.getUser(anyString(), anyString()))
        .thenThrow(new GoneException(UserError.ALREADY_DELETED));

    customProvider.authenticate(new CredentialsToken("Basic", "username:password"));
  }

  @Test(expected = UsernameNotFoundException.class)
  public void authenticateBasicTest_Unauthorized() {
    when(userService.getUser(anyString(), anyString()))
        .thenThrow(new UnauthorizedException(UserError.UNAUTHORIZED));

    customProvider.authenticate(new CredentialsToken("Basic", "username:password"));
  }

  @Test
  public void authenticateAnonymousTest() {
    Authentication authentication =
        customProvider.authenticate(new CredentialsToken("Anonymous", ""));

    assertTrue(authentication instanceof UsernamePasswordAuthenticationToken);
    UsernamePasswordAuthenticationToken casted =
        (UsernamePasswordAuthenticationToken) authentication;
    assertEquals("anonymous", ((UserDetails) casted.getPrincipal()).getUsername());
    assertEquals(
        ImmutableList.of(Authority.ANONYMOUS),
        ((UserDetails) casted.getPrincipal()).getAuthorities());
  }
}
