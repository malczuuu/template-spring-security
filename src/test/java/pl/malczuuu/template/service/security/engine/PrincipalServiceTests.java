package pl.malczuuu.template.service.security.engine;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import pl.malczuuu.template.service.common.error.GoneException;
import pl.malczuuu.template.service.common.error.NotFoundException;
import pl.malczuuu.template.service.security.UserAssembler;
import pl.malczuuu.template.service.security.UserDto;
import pl.malczuuu.template.service.security.UserError;
import pl.malczuuu.template.service.security.UserMapper;
import pl.malczuuu.template.service.security.UserService;

public class PrincipalServiceTests {

  private final UserService userService = mock(UserService.class);
  private final PrincipalService principalService = new PrincipalService(userService);

  @Test
  public void loadUserByUsernameTest() {
    UserDto user = new UserMapper().map(UserAssembler.random());
    when(userService.getUser(any())).thenReturn(user);

    Principal principal = principalService.loadUserByUsername(user.getUsername());

    assertEquals(principal.getUsername(), user.getUsername());
  }

  @Test(expected = UsernameNotFoundException.class)
  public void loadUserByUsernameTest_NotFound() {
    when(userService.getUser(anyString())).thenThrow(new NotFoundException(UserError.NOT_FOUND));

    principalService.loadUserByUsername("username");
  }

  @Test(expected = UsernameNotFoundException.class)
  public void loadUserByUsernameTest_Gone() {
    when(userService.getUser(anyString())).thenThrow(new GoneException(UserError.ALREADY_DELETED));

    principalService.loadUserByUsername("username");
  }
}
