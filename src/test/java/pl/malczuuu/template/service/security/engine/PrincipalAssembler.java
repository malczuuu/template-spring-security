package pl.malczuuu.template.service.security.engine;

import com.google.common.collect.ImmutableList;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCrypt;
import pl.malczuuu.template.service.security.Role;
import pl.malczuuu.template.service.security.User;

public class PrincipalAssembler {

  public static UserDetails admin() {
    return new Principal(
        "root",
        BCrypt.hashpw("root", BCrypt.gensalt()),
        true,
        true,
        true,
        true,
        ImmutableList.copyOf(Role.ADMIN.getAuthorities()));
  }

  public static UserDetails of(User user) {
    return new Principal(
        user.getUsername(),
        user.getPassword(),
        true,
        true,
        true,
        true,
        ImmutableList.copyOf(Role.USER.getAuthorities()));
  }

  public static UserDetails random() {
    return new Principal(
        RandomStringUtils.randomAlphabetic(10),
        BCrypt.hashpw(RandomStringUtils.randomAlphabetic(10), BCrypt.gensalt()),
        true,
        true,
        true,
        true,
        ImmutableList.copyOf(Role.USER.getAuthorities()));
  }
}
