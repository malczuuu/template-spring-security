package pl.malczuuu.template.service.security;

import java.time.Instant;
import java.util.concurrent.ThreadLocalRandom;

public class SessionAssembler {
  public static Session root() {
    return new Session(
        ThreadLocalRandom.current().nextLong(),
        RandomToken.sessionToken(),
        UserAssembler.admin(),
        Instant.now().plusSeconds(60 * 60 * 24),
        Instant.now());
  }
}
