package pl.malczuuu.template.service.security;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class UserMapperTests {

  private final UserMapper userMapper = new UserMapper();

  @Test
  public void mapTest() {
    User user = UserAssembler.admin();
    UserDto dto = userMapper.map(user);
    assertEquals(user.getUsername(), dto.getUsername());
    assertEquals(user.getEmail(), dto.getEmail());
    assertEquals(user.getFirstName(), dto.getFirstName());
    assertEquals(user.getLastName(), dto.getLastName());
    assertEquals(user.getPublicEmail(), dto.getPublicEmail());
    assertEquals(user.getRole(), dto.getRole());
  }
}
