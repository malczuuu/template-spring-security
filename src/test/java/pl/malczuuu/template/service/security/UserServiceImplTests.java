package pl.malczuuu.template.service.security;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.google.common.collect.ImmutableList;
import java.util.Optional;
import org.junit.Test;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCrypt;
import pl.malczuuu.template.service.common.error.BadRequestException;
import pl.malczuuu.template.service.common.error.ConflictException;
import pl.malczuuu.template.service.common.error.GoneException;
import pl.malczuuu.template.service.common.error.NotFoundException;
import pl.malczuuu.template.service.common.error.UnauthorizedException;
import pl.malczuuu.template.service.security.engine.PrincipalAssembler;

public class UserServiceImplTests {

  private final UserRepository userRepository = mock(UserRepository.class);
  private final UserServiceImpl userService = new UserServiceImpl(userRepository);

  @Test
  public void getPageOfUsersTest() {
    Page<User> users =
        new PageImpl<>(
            ImmutableList.of(
                UserAssembler.random(),
                UserAssembler.random(),
                UserAssembler.random(),
                UserAssembler.random(),
                UserAssembler.random(),
                UserAssembler.random()));
    when(userRepository.findAllByDeleted(anyBoolean(), any())).thenReturn(users);

    Page<UserDto> dtos = userService.getPageOfUsers(PageRequest.of(0, 6));

    assertEquals(users.getTotalElements(), dtos.getTotalElements());
    users
        .stream()
        .forEach(
            u ->
                assertTrue(dtos.stream().anyMatch(d -> d.getUsername().equals(u.getUsername()))));
  }

  @Test
  public void getUserByUsernameTest() {
    User user = UserAssembler.random();
    when(userRepository.findByUsername(anyString())).thenReturn(Optional.of(user));

    UserDto dto = userService.getUser(user.getUsername());

    assertEquals(user.getUsername(), dto.getUsername());
  }

  @Test(expected = NotFoundException.class)
  public void getUserByUsernameTest_NotFound() {
    when(userRepository.findByUsername(anyString())).thenReturn(Optional.empty());

    userService.getUser("username");
  }

  @Test(expected = GoneException.class)
  public void getUserByUsernameTest_Gone() {
    User user = UserAssembler.random();
    user.setDeleted(true);
    when(userRepository.findByUsername(anyString())).thenReturn(Optional.of(user));

    userService.getUser(user.getUsername());
  }

  @Test
  public void getUserByUsernameAndPasswordTest() {
    String password = "password";
    User user = UserAssembler.random();
    user.setPassword(BCrypt.hashpw(password, BCrypt.gensalt()));
    when(userRepository.findByUsername(anyString())).thenReturn(Optional.of(user));

    UserDto dto = userService.getUser(user.getUsername(), password);

    assertEquals(user.getUsername(), dto.getUsername());
  }

  @Test(expected = NotFoundException.class)
  public void getUserByUsernameAndPasswordTest_NotFound() {
    when(userRepository.findByUsername(anyString())).thenReturn(Optional.empty());

    userService.getUser("username", "password");
  }

  @Test(expected = GoneException.class)
  public void getUserByUsernameAndPasswordTest_Gone() {
    String password = "password";
    User user = UserAssembler.random();
    user.setPassword(BCrypt.hashpw(password, BCrypt.gensalt()));
    user.setDeleted(true);
    when(userRepository.findByUsername(anyString())).thenReturn(Optional.of(user));

    userService.getUser(user.getUsername(), password);
  }

  @Test
  public void createUserAsRootTest() {
    UserToCreateDto userToCreate =
        new UserToCreateDto(
            "username",
            "password",
            "User",
            "User",
            "username@example.com",
            Role.USER);
    User user = UserAssembler.random();
    user.setUsername(userToCreate.getUsername());
    user.setPassword(BCrypt.hashpw(userToCreate.getPassword(), BCrypt.gensalt()));
    user.setEmail(userToCreate.getEmail());
    user.setFirstName(userToCreate.getFirstName());
    user.setLastName(userToCreate.getLastName());
    when(userRepository.existsByUsername(anyString())).thenReturn(false);
    when(userRepository.existsByEmail(anyString())).thenReturn(false);
    when(userRepository.save(any())).thenReturn(user);

    UserDto dto = userService.createUser(userToCreate);

    assertNotNull(dto);
  }

  @Test(expected = ConflictException.class)
  public void createUserAsRootTest_UsernameConflict() {
    UserToCreateDto userToCreate =
        new UserToCreateDto(
            "username",
            "password",
            "User",
            "User",
            "username@example.com",
            Role.USER);
    when(userRepository.existsByUsername(anyString())).thenReturn(true);
    when(userRepository.existsByEmail(anyString())).thenReturn(false);

    userService.createUser(userToCreate);
  }

  @Test(expected = ConflictException.class)
  public void createUserAsRootTest_EmailConflict() {
    UserToCreateDto userToCreate =
        new UserToCreateDto(
            "username",
            "password",
            "User",
            "User",
            "username@example.com",
            Role.USER);
    when(userRepository.existsByUsername(anyString())).thenReturn(false);
    when(userRepository.existsByEmail(anyString())).thenReturn(true);

    userService.createUser(userToCreate);
  }

  @Test
  public void updateUserAsRootTest() {
    User user = UserAssembler.random();
    UserDetailsDto userDetails =
        new UserDetailsDto(
            "updated@example.com",
            "First",
            "Name",
            "public@example.com",
            Role.USER);
    UserDetails principal = PrincipalAssembler.admin();
    when(userRepository.findByUsername(anyString())).thenReturn(Optional.of(user));
    when(userRepository.existsByUsername(anyString())).thenReturn(false);
    when(userRepository.existsByEmail(anyString())).thenReturn(false);
    when(userRepository.save(any())).thenReturn(user);

    UserDto dto = userService.updateUser(user.getUsername(), userDetails, principal);

    assertEquals(userDetails.getEmail(), dto.getEmail());
    assertEquals(userDetails.getFirstName(), dto.getFirstName());
    assertEquals(userDetails.getLastName(), dto.getLastName());
    assertEquals(userDetails.getPublicEmail(), dto.getPublicEmail());
    assertEquals(userDetails.getRole(), dto.getRole());
  }

  @Test(expected = NotFoundException.class)
  public void updateUserAsRootTest_NotFound() {
    UserDetails principal = PrincipalAssembler.admin();
    UserDetailsDto userDetails =
        new UserDetailsDto(
            "updated@example.com",
            "First",
            "Name",
            "public@example.com",
            Role.USER);
    when(userRepository.existsByUsername(anyString())).thenReturn(false);
    when(userRepository.existsByEmail(anyString())).thenReturn(false);
    when(userRepository.findByUsername(anyString())).thenReturn(Optional.empty());

    userService.updateUser("username", userDetails, principal);
  }

  @Test(expected = GoneException.class)
  public void updateUserAsRootTest_Gone() {
    User user = UserAssembler.random();
    user.setDeleted(true);
    UserDetails principal = PrincipalAssembler.admin();
    UserDetailsDto userDetails =
        new UserDetailsDto(
            "updated@example.com",
            "First",
            "Name",
            "public@example.com",
            Role.USER);
    when(userRepository.existsByUsername(anyString())).thenReturn(false);
    when(userRepository.existsByEmail(anyString())).thenReturn(false);
    when(userRepository.findByUsername(anyString())).thenReturn(Optional.of(user));

    userService.updateUser(user.getUsername(), userDetails, principal);
  }

  @Test(expected = ConflictException.class)
  public void updateUserAsRootTest_EmailConflict() {
    User user = UserAssembler.random();
    UserDetails principal = PrincipalAssembler.admin();
    UserDetailsDto userDetails =
        new UserDetailsDto(
            "updated@example.com",
            "First",
            "Name",
            "public@example.com",
            Role.USER);
    when(userRepository.existsByUsername(anyString())).thenReturn(false);
    when(userRepository.existsByEmail(anyString())).thenReturn(true);
    when(userRepository.findByUsername(anyString())).thenReturn(Optional.of(user));

    userService.updateUser(user.getUsername(), userDetails, principal);
  }

  @Test
  public void updateUserAsNotRootTest() {
    User user = UserAssembler.random();
    UserDetails principal = PrincipalAssembler.of(user);
    UserDetailsDto userDetails =
        new UserDetailsDto(
            "updated@example.com", "First", "Name", "public@example.com", Role.USER);
    when(userRepository.existsByUsername(anyString())).thenReturn(false);
    when(userRepository.existsByEmail(anyString())).thenReturn(false);
    when(userRepository.findByUsername(anyString())).thenReturn(Optional.of(user));
    when(userRepository.save(any())).thenReturn(user);

    UserDto dto = userService.updateUser(user.getUsername(), userDetails, principal);

    assertEquals(dto.getUsername(), user.getUsername());
  }

  @Test(expected = UnauthorizedException.class)
  public void updateUserAsNotRootTest_AccessDenied() {
    User user = UserAssembler.random();
    user.setUsername("username");
    UserDetails principal = PrincipalAssembler.random();
    UserDetailsDto userDetails =
        new UserDetailsDto(
            "updated@example.com", "First", "Name", "public@example.com", Role.USER);
    when(userRepository.existsByUsername(anyString())).thenReturn(false);
    when(userRepository.existsByEmail(anyString())).thenReturn(false);
    when(userRepository.findByUsername(anyString())).thenReturn(Optional.of(user));

    userService.updateUser(user.getUsername(), userDetails, principal);
  }

  @Test(expected = BadRequestException.class)
  public void updateUserAsNotRootTest_ForbiddenFields() {
    User user = UserAssembler.random();
    UserDetails principal = PrincipalAssembler.of(user);
    UserDetailsDto userDetails =
        new UserDetailsDto(
            "updated@example.com",
            "First",
            "Name",
            "public@example.com",
            Role.ADMIN);
    when(userRepository.existsByUsername(anyString())).thenReturn(false);
    when(userRepository.existsByEmail(anyString())).thenReturn(false);
    when(userRepository.findByUsername(anyString())).thenReturn(Optional.of(user));

    userService.updateUser(user.getUsername(), userDetails, principal);
  }

  @Test
  public void deleteUserAsRootTest() {
    User user = UserAssembler.random();
    user.setDeleted(false);
    UserDetails principal = PrincipalAssembler.admin();
    when(userRepository.findByUsername(anyString())).thenReturn(Optional.of(user));
    when(userRepository.save(any())).thenReturn(user);

    UserDto dto = userService.deleteUser(user.getUsername(), principal);

    assertEquals(dto.isDeleted(), true);
  }

  @Test(expected = NotFoundException.class)
  public void deleteUserAsRootTest_NotFound() {
    UserDetails principal = PrincipalAssembler.admin();
    when(userRepository.findByUsername(anyString())).thenReturn(Optional.empty());

    userService.deleteUser("username", principal);
  }

  @Test(expected = GoneException.class)
  public void deleteUserAsRootTest_Gone() {
    User user = UserAssembler.random();
    user.setDeleted(true);
    UserDetails principal = PrincipalAssembler.admin();
    when(userRepository.findByUsername(anyString())).thenReturn(Optional.of(user));

    userService.deleteUser(user.getUsername(), principal);
  }
}
