package pl.malczuuu.template.service.security;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Optional;
import org.junit.Test;
import org.springframework.security.crypto.bcrypt.BCrypt;
import pl.malczuuu.template.service.common.error.UnauthorizedException;

public class SessionServiceImplTests {

  private final UserRepository userRepository = mock(UserRepository.class);
  private final SessionRepository sessionRepository = mock(SessionRepository.class);
  private final SessionServiceImpl sessionService =
      new SessionServiceImpl(userRepository, sessionRepository);

  @Test
  public void getSessionTest() {
    Session session = SessionAssembler.root();
    when(sessionRepository.findByTokenAndDeleted(anyString(), anyBoolean()))
        .thenReturn(Optional.of(session));

    SessionDto dto = sessionService.getSession(session.getToken());

    assertEquals(session.getToken(), dto.getToken());
  }

  @Test(expected = UnauthorizedException.class)
  public void getSessionTest_NotFound() {
    when(sessionRepository.findByTokenAndDeleted(anyString(), anyBoolean()))
        .thenReturn(Optional.empty());

    sessionService.getSession("session-token");
  }

  @Test
  public void createSessionTest() {
    String password = "example";
    Session session = SessionAssembler.root();
    session.getUser().setPassword(BCrypt.hashpw(password, BCrypt.gensalt()));
    CredentialsDto credentials =
        new CredentialsDto(session.getUser().getUsername(), password, false);
    when(userRepository.findByUsername(anyString())).thenReturn(Optional.of(session.getUser()));
    when(sessionRepository.save(any())).thenReturn(session);

    SessionDto dto = sessionService.createSession(credentials);

    assertNotNull(dto);
  }

  @Test(expected = UnauthorizedException.class)
  public void createSessionTest_InvalidUsername() {
    CredentialsDto credentials = new CredentialsDto("invalid-username", "password", false);
    when(userRepository.findByUsername(anyString())).thenReturn(Optional.empty());

    sessionService.createSession(credentials);
  }

  @Test(expected = UnauthorizedException.class)
  public void createSessionTest_InvalidPassword() {
    String password = "password";
    Session session = SessionAssembler.root();
    session.getUser().setPassword(BCrypt.hashpw(password, BCrypt.gensalt()));
    CredentialsDto credentials =
        new CredentialsDto(session.getUser().getUsername(), "invalid-password", false);
    when(userRepository.findByUsername(anyString())).thenReturn(Optional.of(session.getUser()));

    sessionService.createSession(credentials);
  }

  @Test
  public void deleteSessionTest() {
    Session session = SessionAssembler.root();
    when(sessionRepository.findByTokenAndDeleted(anyString(), anyBoolean()))
        .thenReturn(Optional.of(session));
    when(sessionRepository.save(any())).thenReturn(session);

    SessionDto dto = sessionService.deleteSession(session.getToken());

    assertEquals(session.getToken(), dto.getToken());
  }

  @Test(expected = UnauthorizedException.class)
  public void deleteSessionTest_NotFound() {
    when(sessionRepository.findByTokenAndDeleted(anyString(), anyBoolean()))
        .thenReturn(Optional.empty());

    sessionService.deleteSession("session-token");
  }
}
