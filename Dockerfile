# Stage 1: build application
FROM gradle:4.6-jdk8-alpine as builder

RUN mkdir app
ADD . app

RUN cd app/ && gradle clean build

# Stage 2: setup docker image
FROM openjdk:8-jre-alpine

ENV TEMPLATE_DB_HOST=localhost
ENV TEMPLATE_DB_PORT=3306
ENV TEMPLATE_DB_USER=template
ENV TEMPLATE_DB_PASS=template
ENV TEMPLATE_DB_NAME=template
ENV TEMPLATE_SPRING_LOG_LEVEL=info
ENV TEMPLATE_LOG_LEVEL=info

COPY --from=builder /home/gradle/app/build/libs/*.jar app.jar

ENV JAVA_OPTS=""

EXPOSE 8080

CMD java \
  -Djava.security.egd=file:/dev/./urandom \
  -Duser.timezone=UTC \
  -Dserver.address=0.0.0.0 \
  -Dserver.port=8080 \
  -Dspring.datasource.url=jdbc:mysql://${TEMPLATE_DB_HOST}:${TEMPLATE_DB_PORT}/${TEMPLATE_DB_NAME}?useSSL=false \
  -Dspring.datasource.username=${TEMPLATE_DB_USER} \
  -Dspring.datasource.password=${TEMPLATE_DB_PASS} \
  -jar app.jar
