# Template for Spring Security

Template for securing Spring Web using users and sessions in central database. It implements two ways of authentication, Basic Auth and custom token-based authentication. Authentication depends on `Authorization` HTTP header. Securing controllers is done with `@PreAuthorize("hasAuthority('???'))` annotations.

HTTP header form:

* `Authorization: Basic dXNlcjpwYXNzd29yZA==`
* `Authorization: Token Qa32ZxFgE22rwse23csd23`

## Predefined REST API

* **the users API**
```
GET     /users
    Response:  200 Ok, Page<UserDto>
```
```
POST    /users
    Request:   UserToCreateDto
    Response:  201 Created, UserDto
```
```
GET     /users/{username}
    Response:  200 Ok, UserDto
```
```
PUT     /users/{username}
    Request:   UserDataDto
    Response:  200 Ok, UserDto
```
```
DELETE  /users/{username}
    Response:  204 No content
```

* **the sessions API**
```
POST    /sessions
    Request:   CredentialsDto
    Response:  201 Created, SessionDto
```
```
GET     /sessions/{token}
    Response:  200 Ok, SessionDto
```
```
DELETE  /sessions/{token}
    Response:  204 No content
```
